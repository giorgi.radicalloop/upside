import logo from "../images/Logo.png"
import home from "../images/icons/home.png"
import email from "../images/icons/email.png"
import file from "../images/icons/file.png"
import plus from "../images/icons/plus.png"
import buyers from "../images/icons/buyers.png"
import user from "../images/icons/user.png"
export const navIcons = {logo, home, file, email, plus, buyers, user}
