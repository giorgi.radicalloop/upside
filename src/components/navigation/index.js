import React from 'react';
import {navIcons} from "../../exports/exports";
import Styles from "../../css/navigation.module.css"

const {logo, home, file, buyers, email, plus, user} = navIcons
const Navigation = () => {
return (
      <div className={Styles.parent}>
        <div className={Styles.container}>
          <div className="cursor-pointer">
            <img alt="logo" src={logo}/>
          </div>
          <div className={Styles.nav}>
            <div>
              <div>
                <img alt="#" src={home}/>
              </div>
              <h4 className="active-nav">My Properties</h4>
            </div>
            <div>
              <div>
                <img alt="#" src={file}/>
              </div>
              <h4>Offers</h4>
            </div>
            <div>
              <div>
                <img alt="#" src={plus}/>
              </div>
              <h4>Add New Property</h4>
            </div>
            <div>
              <div>
                <img alt="#" src={buyers}/>
              </div>
              <h4>My Buyers</h4>
            </div>
            <div>
              <div>
                <img alt="img" src={email}/>
              </div>
              <h4>Messages</h4>
            </div>
          </div>
        </div>
        <div className={Styles.line}></div>
        <div className={Styles.user}>
          <img alt="#" src={user}/>
          <h4 className="ml-[18px]">aslayson</h4>
        </div>
      </div>
)
}

export default Navigation;