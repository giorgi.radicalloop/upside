import {cardData} from "../../api/data";
import Card from "./card";
const Cards = () => {
  return cardData.map((data, index) => {
    return <Card key={index} quantity={data.quantity} name={data.name} />
  })
};

export default Cards;