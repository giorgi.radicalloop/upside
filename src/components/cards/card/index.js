import Styles from "../../../css/card.module.css"

const Card = ({quantity, name}) => {
  return (
      <div className={`${Styles.card} `}>
        <div>
          <h1 className="text-[25px] sm:text-[32px] text-[#230B41]">{quantity}</h1>
          <span className="text-[#1AC8DB] text-[15px] leading-5">{name}</span>
        </div>
      </div>
  );
};

export default Card;