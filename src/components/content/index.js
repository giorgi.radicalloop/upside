import Styles from "../../css/content.module.css"
import Cards from "../cards";
import Properties from "../properties";

const Content = () => {
  return (
      <div className={Styles.contentParent}>
        <div className="text-[32px] leading-9">Good evening Scott</div>
        <p className="text-[19px] text-[#5C576A] loading mt-3">Here is what's happening with your business today</p>
        <div className={Styles.cards}>
          <Cards/>
        </div>
        <div className={Styles.titlesParent}>
          <div className="activeTitle">Properties</div>
          <div>Offers</div>
          <div>Messages</div>
        </div>
        <div className={`sm:mt-5 mt-[20px]`}>
          <div>
            <Properties/>
          </div>
        </div>
        <div>
        </div>
      </div>
  );
};

export default Content;