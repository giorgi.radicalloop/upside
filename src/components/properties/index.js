import React from 'react';
import Property from "./property/property";
import PropertySm from "./property/mobile-type-property";
import Styles from "../../css/properties.module.css"
import {propertyData, propertyData_mobile} from "../../api/data";

const Properties = () => {
  return (
      <>
        <div className={Styles.properties}>
          <table className={Styles.table}>
            <tbody>
            <tr>
              <th className="pl-[4rem] flex justify-center">Property Address</th>
              <th>Beds</th>
              <th>Baths</th>
              <th>SqFt</th>
              <th>Year Bult</th>
              <th>Price</th>
              <th>Fee Potential</th>
              <th>
                <div className="flex justify-center">Status</div>
              </th>
            </tr>
            {propertyData.map((data, index) => {
              return <Property key={index} data={data}/>
            })}
            </tbody>
          </table>
        </div>
        {/*mobile variant bottom*/}
        <div className={Styles.smProperties}>
          {propertyData_mobile.map((data, index) => {
            return <PropertySm key={index} data={data}/>
          })}
        </div>
      </>
  );
};

export default Properties;