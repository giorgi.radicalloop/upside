import React from 'react';
const PropertySm = ({data}) => {

const {img, address, Status} = data

  return (
      <div className="flex justify-between py-5 items-center border-b-[1px] border-[#E5E2E9]">
        <img src={img}/>
        <div className="text-[#5C576A] ml-3 mr-3 text-[12px]">{address}</div>
        <button
            className={` w-[74px] flex text-[14px] items-center ${Status}Btn justify-center Btn rounded text-white`}>
          {Status}
        </button>
      </div>
  );
};

export default PropertySm;