import pen from "../../../images/icons/pen.png"

const Property = ({data}) => {
  const {
    img,
    address,
    beds,
    Baths,
    SqFt,
    YearBult,
    Price,
    FeePotential,
    Status,
  } = data

  return (
        <tr className={Status === "Active" ? "activeOption" : ""}>
          <td className="flex items-center pl-1">
            <img src={img}/>
            <div className="ml-5 pr-4">
              {address}
            </div>
          </td>
          <td>{beds}</td>
          <td>{Baths}</td>
          <td>{SqFt}</td>
          <td>{YearBult}</td>
          <td>{Price.toLocaleString('en-US')}</td>
          <td>{FeePotential.toLocaleString('en-US')}</td>
          <td>
            <div className="flex justify-center">
              <button className={`px-5 py-2 w-[74px] flex items-center justify-center ${Status}Btn rounded text-white`}>
                {Status}
              </button>
            </div>
            {Status === "Active" ?
                <img className="absolute right-[-20px] top-[50%] translate-y-[-50%]" src={pen}/> : ""}
          </td>
        </tr>
  );
};

export default Property;