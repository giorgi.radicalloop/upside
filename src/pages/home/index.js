import Navigation from "../../components/navigation";
import Content from "../../components/content";
import {useEffect} from "react"
import "../../css/common.css"
import {slide as Menu} from "react-burger-menu";

const Homepage = () => {

  const toggleMenu = ({isOpen}) => {
    const menuWrap = document.querySelector(".bm-menu-wrap");
    isOpen
        ? menuWrap.setAttribute("aria-hidden", false)
        : menuWrap.setAttribute("aria-hidden", true);

  };
  return (
      <div className="flex">
        <div className="desktop-navigation">
          <Navigation/>
        </div>
        <div className="mobile-navigation">
          <Menu noOverlay onStateChange={toggleMenu}>
            <Navigation/>
          </Menu>
        </div>
        <Content/>
      </div>
  );
};

export default Homepage;