import img from "../../src/images/PropertyImage.png"

export const cardData = [{
  quantity: 17, name: "Offers"
}, {
  quantity: 3, name: "Active"
}, {
  quantity: 2, name: "Pending"
}, {
  quantity: "$47,000", name: "Pending Revenue"
}, {
  quantity: "$123,000", name: "YTD Revenue"
},]

export const propertyData = [{
  img: img,
  address: "7241 McVay Manor Cove",
  beds: 4,
  Baths: 3.5,
  SqFt: 2.350,
  YearBult: 1980,
  Price: 145000,
  FeePotential: 32000,
  Status: "Active",
}, {
  img: img,
  address: "7241 McVay Manor Cove",
  beds: 4,
  Baths: 3.5,
  SqFt: 2.350,
  YearBult: 1980,
  Price: 145000,
  FeePotential: 32000,
  Status: "Pending",

}, {
  img: img,
  address: "7241 McVay Manor Cove",
  beds: 4,
  Baths: 3.5,
  SqFt: 2.350,
  YearBult: 1980,
  Price: 145000,
  FeePotential: 32000,
  Status: "Close",
}]

export const propertyData_mobile = [{
  img: img, address: "7241 McVay Manor Cove Germantown, TN 38138", Status: "Active",
}, {
  img: img, address: "7241 McVay Manor Cove Germantown, TN 38138", Status: "Pending",

}, {
  img: img, address: "7241 McVay Manor Cove Germantown, TN 38138", Status: "Close",
}]